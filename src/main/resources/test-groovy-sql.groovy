Class.forName("com.mysql.cj.jdbc.Driver")
def url = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=false"
def username = "root"
def password = "123456"
groovy.sql.Sql.withInstance(url,username,password,"com.mysql.cj.jdbc.Driver"){
    it.query([codes:['code1','code2']],'select * from t_dept where code in (:codes)'){
        println(it.id)
    }
}