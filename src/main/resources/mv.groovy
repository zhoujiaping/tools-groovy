def sourceDir = 'e:/downloads'
def targetDir = 'e:/mp3'

new File(sourceDir).eachFile{
    if(it.name.endsWith('.mp3')){
        new File(targetDir,it.name).bytes = it.bytes
        it.delete()
    }
}