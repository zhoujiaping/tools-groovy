import com.sun.net.httpserver.HttpServer
import groovy.io.FileType
import org.apache.commons.compress.archivers.tar.TarArchiveEntry
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream

def server = HttpServer.create(new InetSocketAddress(8001), 0)
server.createContext("/test") {
    try{
        println it.getRequestURI()
        def uri = it.getRequestURI()
        def params = parseUriParams(uri)
        println "params=>$params"
        def sourceDir = params['sourceDir']
        def dir = new File(sourceDir)

        //it.responseHeaders.set("Content-Type", "text/plain")
        //it.sendResponseHeaders(200, bytes.length)
        //it.responseBody.bytes = bytes
        def bytes = tar(dir)

        it.responseHeaders.set("Content-Type","application/download")// 提示用户将当前文件保存到本地。
        it.responseHeaders.set("Content-Disposition","attachment;filename=${dir.name}.tar")
        it.responseHeaders.set("Content-Transfer-Encoding","binary")
        it.sendResponseHeaders(200,bytes.length)
        def output = it.responseBody
        output.bytes = bytes
        output.close()
    }catch(e){
        e.printStackTrace()
        it.sendResponseHeaders(500,e.message.bytes.length)
        it.responseBody.bytes = e.message.bytes
    }
}
server.start()

Map<String, String> parseUriParams(URI uri) {
    def query = uri.query
    def params = [:]
    def kvs = query.split('&')
    kvs.each {
        kv ->
            def eqIdx = kv.indexOf('=')
            if (eqIdx > -1) {
                def key = kv.substring(0, eqIdx)
                def value = kv.substring(eqIdx+1)
                params[key] = value
            } else {
                params[kv] = null
            }
    }
    params
}
Byte[] tar(File sourceFile){
    def bos = new ByteArrayOutputStream()
    def tarOutput = new TarArchiveOutputStream(bos)
    sourceFile.eachFileRecurse(FileType.ANY){
        def entry = new TarArchiveEntry(it)
        tarOutput.putArchiveEntry(entry)
        if(it.isFile()){
            tarOutput.write(it.bytes)
        }
        tarOutput.closeArchiveEntry()
    }
    tarOutput.close()
    bos.toByteArray()
}