import groovy.sql.Sql

/**
 * 对比两个环境的数据库的表、字段、索引。
 * +：表示开发环境存在，测试环境不存在。从开发环境同步到测试环境时，需要在测试环境添加。
 * -：表示开发环境不存在，测试环境存在。从开发环境同步到测试环境时，需要在测试环境删除。
 * u：表示开发环境和测试环境都存在，但存在差异。
 */

def config = [
        dbDriver: 'com.mysql.jdbc.Driver',
        tableNameLike : '%md%',
        dev     : [
                dbUrl   : 'jdbc:mysql://localhost:3306/db1?useUnicode=true&characterEncoding=utf-8&useSSL=false',
                dbUser  : '',
                dbPass  : '',
                dbSchema: '',
        ],
        test    : [
                dbUrl   : 'jdbc:mysql://localhost:3306/db2?useUnicode=true&characterEncoding=utf-8&useSSL=false',
                dbUser  : '',
                dbPass  : '',
                dbSchema: '',
        ]
]

def collectTables(config, env) {
    def client = Sql.newInstance(config[env].dbUrl, config[env].dbUser, config[env].dbPass, config.dbDriver)

    def tables = [:]
    client.eachRow("""SELECT * FROM information_schema.tables WHERE TABLE_SCHEMA = ? and table_name like ?;""", [config[env].dbSchema, config.tableNameLike]) {
        //println it
        tables[it.table_name] = [
                table_name     : it.table_name,
                engine         : it.engine,
                table_collation: it.table_collation,
                table_comment  : it.table_comment,
                columns        : [:],
                indexs         : [:],
        ]
    }
    tables.each {
        entry ->
            client.eachRow("""SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ? order by ordinal_position asc;""", [config[env].dbSchema, entry.key]) {
                entry.value.columns[it.column_name] = [
                        column_name     : it.column_name,
                        //ordinal_position: it.ordinal_position,
                        column_default  : it.column_default,
                        is_nullable     : it.is_nullable,
                        column_type     : it.column_type,
                        column_comment  : it.column_comment,
                ]
            }
            def indexs = [:]
            client.eachRow("""SELECT * FROM INFORMATION_SCHEMA.STATISTICS WHERE table_schema=? and table_name = ? order by index_name,seq_in_index asc;""", [config[env].dbSchema, entry.key]) {
                def index = [
                        index_name  : it.index_name,
                        non_unique  : it.non_unique,
                        seq_in_index: it.seq_in_index,
                        column_name : it.column_name,
                        index_type  : it.index_type,
                ]
                def indexColl = indexs[it.index_name]
                if (!indexColl) {
                    indexColl = []
                    indexs[it.index_name] = indexColl
                }
                indexColl << index
            }
    }
}

def devTables = collectTables(config, 'dev')
def testTables = collectTables(config, 'test')

println "dev ============================> test"
def tableDiff = diffTable(devTables, testTables)
tableDiff.each{
    println it
}

def diffTable(def tables1, def tables2) {
    def differences = [:]
    def adds = tables1.findAll { !tables2.containsKey(it.key) }*.key
    def subs = tables2.findAll { !tables1.containsKey(it.key) }*.key
    def possibleUpdates = tables1.findAll { tables2.containsKey(it.key) }
    if(adds)differences['+'] = adds
    if(subs)differences['-'] = subs
    //println possibleUpdates*.key
    def updates = possibleUpdates.findAll { it.value != tables2[it.key] }.collect {
        def t1 = it.value
        def t2 = tables2[it.key]
        def ups = [:]
        t1*.key.each { col ->
            if (col == 'columns') {
                ups[col] = diffColumns(t1[col], t2[col])
            } else if (col == 'indexs') {
                def indexDiff = diffIndexs(t1[col], t2[col])
                if(indexDiff)
                    ups[col] = indexDiff
            } else if (t1[col] != t2[col]) {
                ups[col] = "${t1[col]} => ${t2[col]}"
            }
        }
        [(it.key):ups]
    }
    if(updates)differences['u'] = updates
    differences
}

def diffColumns(def columns1, def columns2) {
    def differences = [:]
    def adds = columns1.findAll { !columns2.containsKey(it.key) }*.key
    def subs = columns2.findAll { !columns1.containsKey(it.key) }*.key
    def possibleUpdates = columns1.findAll { columns2.containsKey(it.key) }
    if(adds)differences['+'] = adds
    if(subs)differences['-'] = subs
    //println possibleUpdates*.key
    def updates = possibleUpdates.findAll { it.value != columns2[it.key] }.collect {
        def t1 = it.value
        def t2 = columns2[it.key]
        def ups = [:]
        t1*.key.each { col ->
            if (t1[col] != t2[col]) {
                ups[col] = "${t1[col]} => ${t2[col]}"
            }
        }
        [(it.key):ups]
    }
    if(updates)differences['u'] = updates
    differences
}

def diffIndexs(def indexs1, def indexs2) {
    def differences = [:]
    def adds = indexs1.findAll { !indexs2.containsKey(it.key) }*.key
    def subs = indexs2.findAll { !indexs1.containsKey(it.key) }*.key
    def possibleUpdates = indexs1.findAll { indexs2.containsKey(it.key) }
    if(adds)differences['+'] = adds
    if(subs)differences['-'] = subs
    //println possibleUpdates*.key
    def updates = possibleUpdates.findAll { it.value != indexs2[it.key] }.collect {
        def t1 = it.value
        def t2 = indexs2[it.key]
        def ups = [:]
        t1*.key.each { col ->
            if (t1[col] != t2[col]) {
                ups[col] = "${t1[col]} => ${t2[col]}"
            }
        }
        [(it.key):ups]
    }
    if(updates)differences['u'] = updates
    differences
}
