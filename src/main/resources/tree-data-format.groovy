import com.google.gson.GsonBuilder

/**
 * 有时候需要查看json格式树形结构数据，但是数据比较多的时候不方便看数据的层级，需要格式化输出为更易读的格式。
 */

def json = '''
{
    "name":"src",
    "children":[{
        "name":"java",
        "children":[{
            "name":"com"
        },{
            "name":"org"
        }]
    }]
}
'''
def gson = new GsonBuilder().create()
def tree = gson.fromJson(json,Map)
def toStr = {
    Map node,int level->
        def line = node.findAll{it.key!='children'}
        .collect{"${it.key}=${it.value}"}
        .join(',')
        "${level}=>${'---'*level}$line"
}
toStr = {node,level->
    def line = ['id','name','level'].collect{"${it}=${node[it]}"}.join(',')
    "${level}=>${'---'*level}$line"
}
def format(Map tree,toStr,level=0){
    def strs = tree['children'].collect{
        format(it,toStr,level+1)
    }.flatten()
    strs.add(0,toStr(tree,level))
    strs
}
println format(tree,toStr).join('\n')