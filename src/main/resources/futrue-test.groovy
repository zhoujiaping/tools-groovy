import java.time.LocalDateTime
import java.util.concurrent.ArrayBlockingQueue
import java.util.concurrent.Callable
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

def pool = new ThreadPoolExecutor(1,1,1, TimeUnit.DAYS,new ArrayBlockingQueue<Runnable>(1))
def future = pool.submit({
    println "execute task begin at ${LocalDateTime.now()}"
    Thread.sleep(2*1000)
    println "execute task end at ${LocalDateTime.now()}"
    "done"
} as Callable)
Thread.sleep(2*1000)
println LocalDateTime.now()
println future.get()
println LocalDateTime.now()
println future.get()
println LocalDateTime.now()