import groovy.sql.Sql

import java.util.stream.IntStream

def url = 'jdbc:mysql://localhost:3306/test'
def user = 'root'
def pass = '123456'
def client = Sql.newInstance(url, user, pass, 'com.mysql.jdbc.Driver')

client.execute("""
create table if not exists t_dept(
    id bigint(20) primary key auto_increment,
    name varchar(64),
    code varchar(64),
    org_id bigint(20),
    remark varchar(128),
    create_time datetime default current_timestamp,
    creator bigint(20),
    updater bigint(20),
    last_update_time datetime default current_timestamp
)
""")
//client.execute("""
//alter table t_dept add index t_dept_idx1(org_id);
//""")
def random = new Random(10000)
def orgIdIter = IntStream.iterate(0, it->random.nextInt(10000)).iterator()
(0..<3000).each{batch->
    def values = (0..<200).collect{
       ["name${batch*1000+it}","code${batch*1000+it}",orgIdIter.next(),"remark${batch*1000+it}",it,it]
    }
    client.executeInsert("""
            insert into t_dept(name,code,org_id,remark,creator,updater)
            values(
                ${values.collect{
                    it.collect{"?"}.join(",")
                }.join("),(")}
            );
        """.toString(),values.flatten().collect{it.toString()})
}
println "end..."
