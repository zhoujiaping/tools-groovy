import java.util.stream.IntStream

def random = new Random(10000)
def range = (5000..0)
//range = (0..500)
def orgIds = range.collect{
    2000+it
}.join(',')

println """
select * from t_dept where org_id in ($orgIds)
order by org_id limit 100 offset 1000;
"""
groovy.sql.Sql.withInstance('','','',''){
    it.eachRow('',[:]){
        row->
            println(row)
    }
    it.withBatch(3,""){
        it.addBatch(1,2,3)
    }
}
