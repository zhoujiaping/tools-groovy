import groovy.sql.Sql

def url = 'jdbc:mysql://localhost:3306/my-test?useUnicode=true&characterEncoding=utf-8&useSSL=false'
def user = 'root'
def pwd = '123456'
def client = Sql.newInstance(url,user,pwd,'com.mysql.jdbc.Driver')
def columns = 'id,name,age'.split(/\s*,\s*/)
def content = [columns.join(',')]
client.eachRow("select * from test"){
    rs->
        content << columns.collect{
            name->rs[name]
        }.join(',')
}
new File('/test.csv').setText(content.join('\n'),'gbk')
