def code = """
/** 姓名 */
private String name;
"""
println code.replaceAll(/(?s)\/\*\*\s*([^*]*?)\s*\*\/\s*?private ([a-zA-Z]*) ([a-zA-Z]*);/,'''\
    @ApiModelProperty(value = "$1", required = false)
    private $2 $3;
'''
)