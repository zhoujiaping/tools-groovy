import com.alibaba.druid.sql.parser.SQLStatementParser
import com.alibaba.druid.sql.visitor.VisitorFeature

/**
 * 将mybatis打印的sql和参数转换为可在客户端执行的sql。
 * */
def log = '''


==>  Preparing: SELECT * from xxx where x=? and y=? order by z ASC LIMIT 1000
==> Parameters: 3(Long), 5802(String)


'''
def pattern = ~/(?s)^.*?Preparing:(.*?)==> Parameters:(.*?)(<==.*)?$/
def matcher = pattern.matcher(log)
matcher.find()
def sql = matcher.group(1)
def params = matcher.group(2)
def paramList = params.split(/, /).collect{
    def pair = it.trim().split(/\(/)
    if(pair.length>1){
        pair[1] = pair[1][0..-2]
        [value:pair[0],type:pair[1]]
    }else{
        [value:pair[0],type:'Object']
    }
}.collect{
    if(it.type in ['Long','Integer','BigDecimal','Short','Byte','Object']){
        it.value
    }else{
        "'${it.value}'"
    }
}
def executableSql = interleave(sql.split(/\?/),paramList).join('')

println executableSql

def parser = new SQLStatementParser(executableSql)
println parser.parseStatement().toString(VisitorFeature.OutputPrettyFormat)
/**
 * 参考clojure的interleave函数
 * */
def interleave(coll1, coll2){
    def coll = []
    def iter1 = coll1.iterator()
    def iter2 = coll2.iterator()
    while(iter1.hasNext()){
        coll << iter1.next()
        if(iter2.hasNext()){
            coll << iter2.next()
        }
    }
    while(iter2.hasNext()){
        coll << iter2.next()
    }
    coll
}