/**
 * 根据表，生成各层代码
 * 包结构示例
 * org.sirenia.projectname.modulename
 * org.sirenia.projectname.modulename.controller
 * org.sirenia.projectname.modulename.service
 * org.sirenia.projectname.modulename.service.impl
 * org.sirenia.projectname.modulename.mapper
 * org.sirenia.projectname.modulename.dto(XXXCreationDTO/XXXModificationDTO/XXXQueryDTO)
 */
import groovy.sql.Sql

import java.time.LocalDateTime

def config = [
        db_url  : 'jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=false',
        db_user : 'root',
        db_pass : '123456',
        schema  : 'test',
        tables  : ['t_user'],
        roots   : [
                'sirenia'     : 'd:/test/demo/',
        ],
        //按功能分包
        function: 'demo',
        author  : 'sirenia',
        //是否强制覆盖
        overlay : [
                'entity'         : true,
                'creationDTO'    : false,
                'modificationDTO': false,
                'queryDTO'       : false,
                'mapperXml'      : false,
                'mapper'         : false,
                'repository'     : false,
                'repositoryImpl' : false,
                'service'        : false,
                'serviceImpl'    : false,
                'controller'     : false,
        ],
]

Sql client = Sql.newInstance(config.db_url, config.db_user, config.db_pass, 'com.mysql.jdbc.Driver')

def javaTypes = [
        'int'     : 'Integer',
        'bigint'  : 'Long',
        'varchar' : 'String',
        'decimal' : 'BigDecimal',
        'datetime': 'Date',
        'tinyint' : 'Integer',
]
def jdbcTypes = [
        'int'     : 'INTEGER',
        'bigint'  : 'DECIMAL',
        'varchar' : 'VARCHAR',
        'decimal' : 'DECIMAL',
        'datetime': 'DATE',
        'tinyint' : 'DECIMAL',
]

def root = config.roots[config.author]
def function = config.function

def mapperPkg = "org.sirenia.myproject.${function}.mapper"
def entityPkg = "org.sirenia.myproject.${function}.entity"
def dtoPkg = "org.sirenia.myproject.${function}.dto"
def servicePkg = "org.sirenia.myproject.${function}.service"
def serviceImplPkg = "org.sirenia.myproject.${function}.service.impl"
def controllerPkg = "org.sirenia.myproject.${function}.controller"

def toProp(String name) {
    def parts = name.toLowerCase().split(/_/)
    if (parts.size() == 1) {
        parts[0]
    } else {
        parts[0] + parts[1..-1].collect { it.capitalize() }.join('')
    }
}

config.tables.each {
    tableName ->
        println "=" * 120
        def modelName = tableName.split(/_/)[1..-1].collect { it.capitalize() }.join('')
        def now = LocalDateTime.now().format('yyyy-MM-dd HH:mm:ss')
        def author = config.author

        def tableComment
        client.eachRow("""SELECT t.* FROM information_schema.tables t
WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ?;""", [config.schema, tableName]) {
            tableComment = it.table_comment
        }

        def columns = []
        client.eachRow("""
SELECT t.* FROM information_schema.COLUMNS t
WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ? order by t.ordinal_position asc;
""", [config.schema, tableName]) {
            if (it['column_name'] != 'id') {
                columns << [
                        column_name   : it.column_name,
                        data_type     : it.data_type,
                        column_comment: it.column_comment
                ]
            }
        }
        /************************************* entity  *******************************************/
        def ignoreColumns = ['created_by', 'creation_date', 'last_updated_by', 'last_update_date', 'object_version_number']
        def entityColumns = columns.findAll {
            !ignoreColumns.contains(it.column_name.toLowerCase())
        }
        def fields = entityColumns.collect {
            """\
    /** ${it.column_comment} */
    private ${javaTypes[it.data_type]} ${toProp(it.column_name)};"""
        }.join('\n')
        def entityCode = """\
package $entityPkg;

import javax.persistence.Id;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigDecimal;
import lombok.Data;
import java.util.Date;

/**
 * $tableComment
 * @author $author $now
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Table(name = "$tableName")
@Data
public class $modelName {
    @Id
    /** 主键 */
    private Long id;
$fields
}
"""
        def entityFile = new File(root, "src/main/java/${entityPkg.split(/\./).join('/')}/${modelName}.java")
        entityFile.parentFile.mkdirs()
        if (!entityFile.exists() || config.overlay['entity']) {
            entityFile.text = entityCode
            println "${entityFile}: 1"
        }
/************************************* creationDTO  *******************************************/
        def props = entityColumns.collect {
            """\
    @ApiModelProperty(value="${it.column_comment}",required=false)
    private ${javaTypes[it.data_type]} ${toProp(it.column_name)};"""
        }.join('\n')
        def creationDTOCode = """\
package $dtoPkg;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigDecimal;
import lombok.Data;
import java.util.Date;

/**
 * $tableComment
 * @author $author $now
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Data
@ApiModel
public class ${modelName}CreationDTO {
${props}
}
"""
        def creationDTOFile = new File(root, "src/main/java/${entityPkg.split(/\./).join('/')}/${modelName}CreationDTO.java")
        creationDTOFile.parentFile.mkdirs()
        if (!creationDTOFile.exists() || config.overlay['creationDTO']) {
            creationDTOFile.text = creationDTOCode
            println "${creationDTOFile}: 1"
        }
/************************************* modificationDTO  *******************************************/
        def modificationDTOCode = """\
package $dtoPkg;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigDecimal;
import lombok.Data;
import java.util.Date;

/**
 * $tableComment
 * @author $author $now
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Data
@ApiModel
public class ${modelName}ModificationDTO {
    @ApiModelProperty(value="主键",required=true)
    private Long id;
    @ApiModelProperty(value="乐观锁版本号",required=true)
    private Long objectVersionNumber;
${props}
}
"""
        def modificationDTOFile = new File(root, "src/main/java/${entityPkg.split(/\./).join('/')}/${modelName}ModificationDTO.java")
        modificationDTOFile.parentFile.mkdirs()
        if (!modificationDTOFile.exists() || config.overlay['modificationDTO']) {
            modificationDTOFile.text = modificationDTOCode
            println "${modificationDTOFile}: 1"
        }
        /************************************* queryDTO  *******************************************/
        def queryDTOCode = """\
package $dtoPkg;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonInclude;
import java.math.BigDecimal;
import lombok.Data;
import java.util.Date;

/**
 * $tableComment
 * @author $author $now
 */
@JsonInclude(value = JsonInclude.Include.NON_NULL)
@Data
@ApiModel
public class ${modelName}QueryDTO {
${props}
}
"""
        def queryDTOFile = new File(root, "src/main/java/${entityPkg.split(/\./).join('/')}/${modelName}QueryDTO.java")
        queryDTOFile.parentFile.mkdirs()
        if (!queryDTOFile.exists() || config.overlay['queryDTO']) {
            queryDTOFile.text = queryDTOCode
            println "${queryDTOFile}: 1"
        }
/************************************* xml  *******************************************/
        def results = columns.collect {
            """\
        <result column="${it['column_name']}" property="${toProp(it['column_name'])}" jdbcType="${jdbcTypes[it['data_type']]}"/>"""
        }.join('\n')
        def baseColumns = columns.collect {
            "t.id, t.${it.column_name}"
        }.join(', ')
        def mapperXml = """\
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd" >
<mapper namespace="${mapperPkg}.${modelName}Mapper">
    <!-- 可根据自己的需求，是否要使用 -->
    <resultMap id="BaseResultMap" type="${entityPkg}.${modelName}">
        <id column="id" property="id" jdbcType="BIGINT"/>
${results}
    </resultMap>
    <sql id="BaseColumns">$baseColumns</sql>
</mapper>
"""
        def mapperXmlFile = new File(root, "src/main/resources/mapper/${function}/${modelName}Mapper.xml")
        mapperXmlFile.parentFile.mkdirs()
        if (!mapperXmlFile.exists() || config.overlay['mapperXml']) {
            mapperXmlFile.text = mapperXml
            println "${mapperXmlFile}: 1"
        }
/************************************* mapper  *******************************************/
        def mapperCode = """\
package $mapperPkg;

import ${entityPkg}.$modelName;

/**
 * $tableComment
 * @author $author ${now}
 */
public interface ${modelName}Mapper {

}

"""
        def mapperJavaFile = new File(root, "src/main/java/${mapperPkg.split(/\./).join('/')}/${modelName}Mapper.java")
        mapperJavaFile.parentFile.mkdirs()
        if (!mapperJavaFile.exists() || config.overlay['mapper']) {
            mapperJavaFile.text = mapperCode
            println "${mapperJavaFile}: 1"
        }
        /************************************* service *******************************************/
        def serviceCode =
                """
package $servicePkg;

import ${entityPkg}.${modelName};

/**
 * $tableComment
 *
 * @author $author $now
 */
public interface ${modelName}Service {

    /**
    * ${tableComment}查询参数
    *
    * @param queryDTO ${tableComment}
    * @param pageRequest 分页
    * @return ${tableComment}DTO列表
    */
    Page<${modelName}DTO> list(${modelName}QueryDTO queryDTO, PageRequest pageRequest);

    /**
     *  ${tableComment}详情
     *
     * @param id 主键
     * @return  ${tableComment}DTO
     */
    ${modelName}DTO detail(Long id);

    /**
     * 创建${tableComment}
     *
     * @param creationDTO ${tableComment}
     * @return ${tableComment}
     */
    Long create(${modelName}CreationDTO creationDTO);

    /**
     * 更新${tableComment}
     *
     * @param ${modelName.uncapitalize()} ${tableComment}
     * @return ${tableComment}
     */
    Integer update(${modelName}ModificationDTO modificationDTO);

    /**
     * 批量删除${tableComment}
     *
     * @param ids
     */
    Integer remove(String ids);
}

"""
        def serviceFile = new File(root, "src/main/java/${servicePkg.split(/\./).join('/')}/${modelName}Service.java")
        serviceFile.parentFile.mkdirs()
        if (!serviceFile.exists() || config.overlay['service']) {
            serviceFile.text = serviceCode
            println "${serviceFile}: 1"
        }
/************************************* service impl *******************************************/

        def serviceImplCode = """
package $serviceImplPkg;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${entityPkg}.${modelName};
import ${servicePkg}.${modelName}Service;

/**
 * $tableComment
 *
 * @author $author $now
 */
@Service
public class ${modelName}ServiceImpl implements ${modelName}Service {

    @Autowired
    private ${modelName}Mapper ${modelName.uncapitalize()}Mapper;

    @Override
    public Page<${modelName}DTO> list(${modelName}QueryDTO queryDTO, PageRequest pageRequest) {
        return ${modelName.uncapitalize()}Mapper.pageAndSort(pageRequest, queryDTO);
    }

    @Override
    public ${modelName}DTO detail(Long id) {
        return ${modelName.uncapitalize()}Mapper.selectByPrimaryKey(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Long create(${modelName}CreationDTO creationDTO) {
        validObject(${modelName.uncapitalize()});
        ${modelName.uncapitalize()}Mapper.insertSelective(${modelName.uncapitalize()});
        return ${modelName.uncapitalize()};
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer update(${modelName}ModificationDTO modificationDTO) {
        ${modelName.uncapitalize()}Mapper.updateByPrimaryKeySelective(modificationDTO);
        return ${modelName.uncapitalize()};
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer remove(String ids) {
        //TODO
    }
}

"""
        def serviceImplFile = new File(root, "src/main/java/${serviceImplPkg.split(/\./).join('/')}/${modelName}ServiceImpl.java")
        serviceImplFile.parentFile.mkdirs()
        if (!serviceImplFile.exists() || config.overlay['serviceImpl']) {
            serviceImplFile.text = serviceImplCode
            println "${serviceImplFile}: 1"
        }

/************************************* controller *******************************************/

        def controllerCode = """
package $controllerPkg;

import ${servicePkg}.${modelName}Service;
import ${entityPkg}.${modelName};
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

/**
 * $tableComment
 *
 * @author $author $now
 */
@RestController("${modelName.uncapitalize()}Controller" )
@RequestMapping("/${tableName.toLowerCase().split(/_/)[1..-1].join('-')}s" )
public class ${modelName}Controller {

    @Autowired
    private ${modelName}Service ${modelName.uncapitalize()}Service;

    @ApiOperation(value = "${tableComment}列表")
    @GetMapping
    public Page<${modelName}DTO> list(${modelName}QueryDTO queryDTO, PageRequest pageRequest) {
        Page<${modelName}DTO> list = ${modelName.uncapitalize()}Service.list(queryDTO, pageRequest);
        return list;
    }

    @ApiOperation(value = "${tableComment}明细")
    @GetMapping("/{id}")
    public ${modelName}DTO detail(@PathVariable Long id) {
        ${modelName} ${modelName.uncapitalize()} = ${modelName.uncapitalize()}Service.detail(id);
        return ${modelName.uncapitalize()};
    }

    /**
    * 返回主键
    */
    @ApiOperation(value = "创建${tableComment}")
    @PostMapping
    public Long create(@RequestBody ${modelName}CreationDTO creationDTO) {       
        return ${modelName.uncapitalize()}Service.create(creationDTO);
    }

    @ApiOperation(value = "修改${tableComment}")
    @PutMapping
    public Integer update(@RequestBody ${modelName}ModificationDTO modificationDTO) {
        return ${modelName.uncapitalize()}Service.update(modificationDTO);
    }

    @ApiOperation(value = "批量删除${tableComment}")
    @DeleteMapping
    public Integer remove(String ids) {
        return ${modelName.uncapitalize()}Service.remove(ids);
    }

}
"""
        def controllerFile = new File(root, "src/main/java/${controllerPkg.split(/\./).join('/')}/${modelName}Controller.java")
        controllerFile.parentFile.mkdirs()
        if (!controllerFile.exists() || config.overlay['controller']) {
            controllerFile.text = controllerCode
            println "${controllerFile}: 1"
        }
}

println "done!"