import io.swagger.annotations.ApiModel
import util.PackageUtil

/**
 *
 * 检查swagger注解ApiModel是否有相同的value。
 * ps：相同value会导致swagger文档数据错乱。

 */
def classSet = PackageUtil.getClassSet('org.sirenia.xxx',true)
def apiModels = [:]
classSet.each{
    try{
        def clazz = Class.forName(it)
        def apiModel = clazz.getAnnotation(ApiModel)
        if(apiModel){
            def key = apiModel.value()
            def value = apiModels[key]
            if(value==null){
                value = []
                apiModels[key] = value
            }
            value << it
        }
    }catch(e){
        e.printStackTrace()
    }
}
apiModels.each{
    if(it.value.size()>1){
        println "api model same value(${it.key}) for ${it.value}"
    }
}