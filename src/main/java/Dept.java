import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.function.Consumer;

@Data
public class Dept {
    Long id;
    String name;
    String code;
    Integer orgId;
    String remark;
    LocalDateTime createTime;
    Long creator;
    Long updater;
    LocalDateTime lastUpdateTime;
    public Dept tap(Consumer<Dept> fn){
        fn.accept(this);
        return this;
    }

}
