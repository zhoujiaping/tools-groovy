import lombok.Lombok;
import lombok.SneakyThrows;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.core.convert.ConversionService;
import org.springframework.core.convert.support.DefaultConversionService;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * 一个简易的数据库客户端，仅仅依赖spring-core。
 * 支持原生使用问号占位符的sql和命名化sql；
 * eg：select * from t_user where id in (:ids) and name like :nameLike and age between :ageMin and :ageMax;
 * eg: select * from t_user where name like :nameLike;
 * 命名化sql支持bean、map、list作为参数；
 * 原生使用问号占位符的sql支持list作为参数；
 * 支持参数映射；
 * 支持查询结果为bean、map、list；
 * 支持批量新增、批量更新；
 * 新增/批量新增时，可返回新增的主键（如果是有数据库生成主键）；
 * 支持编程式事务；
 * <p>
 * 相比于dbutils，该客户端支持命名参数；
 * 相比于mybatis和spring jdbctemplate，该客户端使用更方便，开箱即用；
 *
 * @author 42121
 */
public class DbCli {
    protected static final Pattern NAMED_PARAM_PATTERN = Pattern.compile("(?<!:):\\w+");
    protected static final Pattern NAMED_PARAM_SQL_PATTERN = Pattern.compile(".*?(?<!:):\\w+");
    protected static final DateTimeFormatter DATE_FMT = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    protected static final DateTimeFormatter DATE_TIME_FMT = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    protected static Map<Class<?>, Map<String, Field>> fieldsCache = new ConcurrentHashMap<>();

    protected ConversionService colValXf = new DefaultConversionService();
    protected Map<Class<?>, Function<Object, Object>> paramMappings = new HashMap<>();
    protected Function<String, String> colNameToPropNameXf = DbCli::toLowerCamel;
    protected Function<String, String> colNameToKeyXf = String::toLowerCase;
    protected Consumer<String> onParsedSqlFn = System.out::println;
    protected Consumer<List<Object>> onCollectActualParamsFn = params ->
            System.out.println(params.stream().map(it -> {
                if (it == null) {
                    return "null";
                } else if (it instanceof Number) {
                    return it.toString();
                } else {
                    return "'" + it + "'";
                }
            }).collect(Collectors.joining(", ")));

    protected DbCli init() {
        paramMappings.put(LocalDate.class, objFn((LocalDate it) -> it.format(DATE_FMT)));
        paramMappings.put(LocalDateTime.class, objFn((LocalDateTime it) -> it.format(DATE_TIME_FMT)));
        return this;
    }

    protected static <P, R> Function<Object, Object> objFn(Function<P, R> f) {
        return (Function<Object, Object>) f;
    }

    protected static ThreadLocal<Connection> connHolder = new ThreadLocal<>();

    public static Connection getConn() {
        return connHolder.get();
    }

    public <T> T withConn(Connection conn, Supplier<T> fn) {
        Connection oldConn = connHolder.get();
        try {
            connHolder.set(conn);
            return fn.get();
        } finally {
            if (oldConn == null) {
                connHolder.remove();
            } else {
                connHolder.set(oldConn);
            }
        }
    }

    public static boolean isNamedParamSql(String sql) {
        return NAMED_PARAM_SQL_PATTERN.matcher(sql).find();
    }

    protected <T> T mappingParam(Object o) {
        if (o == null) {
            return null;
        }
        return (T) paramMappings.getOrDefault(o.getClass(), Function.identity()).apply(o);
    }

    @SneakyThrows
    public static void withInstance(String url, String username, String password, String driver, Consumer<DbCli> fn) {
        Class.forName(driver);
        try (Connection conn = DriverManager.getConnection(url, username, password);) {
            DbCli client = new DbCli().init();
            client.withConn(conn, () -> {
                fn.accept(client);
                return null;
            });
        }
    }

    @SneakyThrows
    protected <T> void eachRow(PreparedStatement ps, List<T> params, Consumer<ResultSet> fn) {
        setStatementParams(ps, params);
        try (ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                fn.accept(rs);
            }
        }
    }

    public void eachRow(String sql, Object params, Consumer<ResultSet> fn) {
        psForParams(sql, params, (ps, paramList) -> eachRow(ps, paramList, fn));
    }

    public void eachRow(String sql, Consumer<ResultSet> fn) {
        psForParams(sql, null, (ps, paramList) -> eachRow(ps, paramList, fn));
    }

    public <T> void eachRowForBean(String sql, Object params, Supplier<T> beanSupplier, Consumer<T> fn) {
        psForParams(sql, params, (ps, paramList) ->
                eachRow(ps, paramList, rs -> fn.accept(rowToBean(rs, beanSupplier)))
        );
    }

    public <T> void eachRowForBean(String sql, Supplier<T> beanSupplier, Consumer<T> fn) {
        eachRowForBean(sql, null, beanSupplier, fn);
    }

    public void eachRowForMap(String sql, Object params, Consumer<Map<String, Object>> fn) {
        psForParams(sql, params, (ps, paramList) ->
                eachRow(ps, paramList, rs -> fn.accept(rowToMap(rs)))
        );
    }

    public void eachRowForMap(String sql, Consumer<Map<String, Object>> fn) {
        eachRowForMap(sql, null, fn);
    }

    public void eachRowForList(String sql, Object params, Consumer<List<Object>> fn) {
        psForParams(sql, params, (ps, paramList) ->
                eachRow(ps, paramList, rs -> fn.accept(rowToList(rs)))
        );
    }

    public void eachRowForList(String sql, Consumer<List<Object>> fn) {
        eachRowForList(sql, null, fn);
    }

    public void eachRowForListWithLabel(String sql, Object params, Consumer<List<Object>> fn) {
        AtomicBoolean collectedHead = new AtomicBoolean(false);
        psForParams(sql, params, (ps, paramList) ->
                eachRow(ps, paramList, rs -> {
                    if (!collectedHead.get()) {
                        collectedHead.set(true);
                        fn.accept((List<Object>) (Object) labels(rs));
                    }
                    fn.accept(rowToList(rs));
                })
        );
    }

    public void eachRowForListWithLabel(String sql, Consumer<List<Object>> fn) {
        eachRowForListWithLabel(sql, null, fn);
    }

    @SneakyThrows
    protected List<String> labels(ResultSet rs) {
        List<String> names = new ArrayList<>();
        ResultSetMetaData meta = rs.getMetaData();
        int count = meta.getColumnCount();
        for (int i = 0; i < count; i++) {
            names.add(colNameToKeyXf.apply(meta.getColumnLabel(i + 1)));
        }
        return names;
    }

    /***
     * eg1:
     * sql is: select * from t_user where id in (:ids) and status=:status
     * params is: [[101,102],'enabled']
     * eg2:
     * sql is: select * from t_user where id in (?.?) and status=?
     * params is: [101,102,'enabled']
     */
    protected void psForListParams(String sql, List<Object> params, BiConsumer<PreparedStatement, List<Object>> fn) {
        if (isNamedParamSql(sql)) {
            psForListNamedParams(sql, params, fn);
        } else {
            psForListActualParams(sql, params, fn);
        }
    }

    @SneakyThrows
    protected void psForListActualParams(String sql, List<Object> params, BiConsumer<PreparedStatement, List<Object>> fn) {
        onParsedSql(sql);
        try (PreparedStatement ps = getConn().prepareStatement(sql)) {
            fn.accept(ps, params == null ? listOf() : params);
        }
    }

    @SneakyThrows
    protected void psForListNamedParams(String sql, List<Object> params, BiConsumer<PreparedStatement, List<Object>> fn) {
        List<Object> actualPrams = new ArrayList<>();
        String actualSql = replaceEachMatched(sql, NAMED_PARAM_PATTERN, (idx, name) -> {
            //eg:  name is :id, params.get("id")
            Object p = params.get(idx);
            if (p instanceof Collection) {
                Collection<Object> c = (Collection<Object>) p;
                StringBuilder holders = new StringBuilder();
                for (Object x : c) {
                    actualPrams.add(x);
                    holders.append("?,");
                }
                if (c.size() > 1) {
                    return holders.deleteCharAt(holders.length() - 1).toString();
                }
                return holders.toString();
            } else {
                actualPrams.add(p);
                return "?";
            }
        });
        onParsedSql(actualSql);
        try (PreparedStatement ps = getConn().prepareStatement(actualSql)) {
            fn.accept(ps, actualPrams);
        }
    }

    @SneakyThrows
    protected void psForMapParams(String sql, Map<String, Object> params, BiConsumer<PreparedStatement, List<Object>> fn) {
        List<Object> actualPrams = new ArrayList<>();
        String actualSql = replaceEachMatched(sql, NAMED_PARAM_PATTERN, (idx, name) -> {
            //eg:  name is :id, params.get("id")
            Object p = params.get(name.substring(1));
            if (p instanceof Collection) {
                Collection<Object> c = (Collection<Object>) p;
                StringBuilder holders = new StringBuilder();
                for (Object x : c) {
                    actualPrams.add(x);
                    holders.append("?,");
                }
                if (c.size() > 1) {
                    return holders.deleteCharAt(holders.length() - 1).toString();
                }
                return holders.toString();
            } else {
                actualPrams.add(p);
                return "?";
            }
        });
        onParsedSql(actualSql);
        try (PreparedStatement ps = getConn().prepareStatement(actualSql)) {
            fn.accept(ps, actualPrams);
        }
    }

    protected void psForParams(String sql, Object params, BiConsumer<PreparedStatement, List<Object>> fn) {
        if (params == null || params instanceof List) {
            psForListParams(sql, (List) params, fn);
        } else if (params instanceof Map) {
            psForMapParams(sql, (Map) params, fn);
        } else {
            psForMapParams(sql, BeanMap.create(params), fn);
        }
    }

    @SneakyThrows
    protected <T> void setStatementParams(PreparedStatement ps, List<T> params) {
        setStatementParams(ps, params, true);
    }

    @SneakyThrows
    protected <T> void setStatementParams(PreparedStatement ps, List<T> params, boolean enableCollParam) {
        if (CollectionUtils.isEmpty(params)) {
            return;
        }
        List<Object> actualParams = new ArrayList<>();
        for (T param : params) {
            if (param instanceof Collection) {
                if (!enableCollParam) {
                    throw new IllegalArgumentException("collection param not enabled!");
                }
                for (Object p : (Collection) param) {
                    actualParams.add(mappingParam(p));
                }
            } else {
                actualParams.add(mappingParam(param));
            }
        }
        onCollectActualParams(actualParams);
        int paramIdx = 1;
        for (Object p : actualParams) {
            ps.setObject(paramIdx++, p);
        }
    }

    protected void onCollectActualParams(List<Object> actualParams) {
        onCollectActualParamsFn.accept(actualParams);
    }

    public <T> T rowToBean(ResultSet rs, Supplier<T> beanSupplier) {
        T bean = beanSupplier.get();
        Map<String, Object> map = rowToMap(rs);
        //use spring BeanWrapper?
        Map<String, Field> fields = fieldMap(bean);
        map.forEach((k, v) -> {
            Field f = fields.get(colNameToPropNameXf.apply(k));
            if (f != null) {
                ReflectionUtils.setField(f, bean, colValXf.convert(v, f.getType()));
            }
        });
        return bean;
    }

    protected <T> Map<String, Field> fieldMap(T bean) {
        Map<String, Field> fields0 = fieldsCache.get(bean.getClass());
        if (fields0 == null) {
            Map<String, Field> fields = new HashMap<>();
            fields0 = fields;
            fieldsCache.put(bean.getClass(), fields);
            ReflectionUtils.doWithFields(bean.getClass(), f -> {
                        fields.put(f.getName(), f);
                        ReflectionUtils.makeAccessible(f);
                    }, f ->
                            !Modifier.isStatic(f.getModifiers()) && !Modifier.isFinal(f.getModifiers())
            );
        }
        return fields0;
    }

    public static String toLowerCamel(String name) {
        char[] chs = name.toLowerCase().toCharArray();
        int i = 0;
        StringBuilder sb = new StringBuilder();
        while (i < chs.length) {
            if (chs[i] == '_') {
                i++;
                if (i < chs.length) {
                    sb.append(Character.toUpperCase(chs[i++]));
                }
            } else {
                sb.append(chs[i++]);
            }
        }
        return sb.toString();
    }

    @SneakyThrows
    public Map<String, Object> rowToMap(ResultSet rs) {
        Map<String, Object> map = new LinkedHashMap<>();
        eachCol(rs, (label, value) -> map.put(colNameToKeyXf.apply(label), value));
        return map;
    }

    @SneakyThrows
    public List<Object> rowToList(ResultSet rs) {
        List<Object> list = new ArrayList<>();
        eachCol(rs, (label, value) -> list.add(value));
        return list;
    }

    public int update(String sql, Object params) {
        AtomicInteger count = new AtomicInteger();
        psForParams(sql, params, (ps, args) -> {
            setStatementParams(ps, args);
            try {
                count.set(ps.executeUpdate());
            } catch (SQLException e) {
                throw Lombok.sneakyThrow(e);
            }
        });
        return count.get();
    }

    public int update(String sql) {
        return update(sql, new ArrayList<>(0));
    }

    public <T> List<T> rowsForBean(String sql, Object params, Supplier<T> beanSupplier) {
        List<T> list = new ArrayList<>();
        eachRowForBean(sql, params, beanSupplier, list::add);
        return list;
    }

    public <T> List<T> rowsForBean(String sql, Supplier<T> beanSupplier) {
        return rowsForBean(sql, listOf(), beanSupplier);
    }

    public List<Map<String, Object>> rowsForMap(String sql, Object params) {
        List<Map<String, Object>> list = new ArrayList<>();
        eachRowForMap(sql, params, list::add);
        return list;
    }

    public List<Map<String, Object>> rowsForMap(String sql) {
        return rowsForMap(sql, null);
    }


    public List<List<Object>> rowsForList(String sql, Object params) {
        List<List<Object>> list = new ArrayList<>();
        eachRowForList(sql, params, list::add);
        return list;
    }

    public List<List<Object>> rowsForList(String sql) {
        return rowsForList(sql, null);
    }

    public List<List<Object>> rowsForListWithLabel(String sql, Object params) {
        List<List<Object>> list = new ArrayList<>();
        eachRowForListWithLabel(sql, params, list::add);
        return list;
    }

    public List<List<Object>> rowsForListWithLabel(String sql) {
        return rowsForListWithLabel(sql, null);
    }

    public <T> T uniqueRowForBean(String sql, Object params, Supplier<T> beanSupplier) {
        Object[] arr = new Object[1];
        eachRowForBean(sql, params, beanSupplier, row -> {
            Assert.isNull(arr[0], "there are more than one row");
            arr[0] = row;
        });
        return (T) arr[0];
    }

    public <T> T uniqueRowForBean(String sql, Supplier<T> beanSupplier) {
        return uniqueRowForBean(sql, null, beanSupplier);
    }

    public Map<String, Object> uniqueRowForMap(String sql, Object params) {
        Object[] arr = new Object[1];
        eachRowForMap(sql, params, row -> {
            Assert.isNull(arr[0], "there are more than one row");
            arr[0] = row;
        });
        return (Map<String, Object>) arr[0];
    }

    public Map<String, Object> uniqueRowForMap(String sql) {
        return uniqueRowForMap(sql, null);
    }

    public List<Object> uniqueRowForList(String sql, Object params) {
        Object[] arr = new Object[1];
        eachRowForList(sql, params, row -> {
            Assert.isNull(arr[0], "there are more than one row");
            arr[0] = row;
        });
        return (List<Object>) arr[0];
    }

    public List<Object> uniqueRowForList(String sql) {
        return uniqueRowForList(sql, null);
    }

    public <E> E uniqueValue(String sql, Object params, Class<E> clazz) {
        List<Object> values = uniqueRowForList(sql, params);
        Assert.isTrue(values.size() <= 1, "expect zero/one column, but found " + values.size());
        return colValXf.convert(values.get(0), clazz);
    }

    public <E> E uniqueValue(String sql, Class<E> clazz) {
        return uniqueValue(sql, null, clazz);
    }

    @SneakyThrows
    public <E> E uniqueValue(ResultSet rs, Class<E> clazz) {
        ResultSetMetaData meta = rs.getMetaData();
        int count = meta.getColumnCount();
        Assert.isTrue(count == 1, "expect one column, but found " + count);
        return colValXf.convert(rs.getObject(1), clazz);
    }

    @SneakyThrows
    public void eachCol(ResultSet rs, BiConsumer<String, Object> fn) {
        ResultSetMetaData meta = rs.getMetaData();
        int count = meta.getColumnCount();
        for (int i = 0; i < count; i++) {
            String label = meta.getColumnLabel(i + 1);
            Object value = rs.getObject(i + 1);
            fn.accept(label, value);
        }
    }

    @SneakyThrows
    public void withTx(Consumer<Connection> fn) {
        Connection conn = getConn();
        boolean autocommit = conn.getAutoCommit();
        try {
            if (autocommit) {
                conn.setAutoCommit(false);
            }
            fn.accept(conn);
        } finally {
            if (autocommit) {
                conn.setAutoCommit(true);
            }
        }
    }

    @SneakyThrows
    public void insert(String sql) {
        insert(sql, listOf(), null);
    }

    @SneakyThrows
    public <K> Optional<K> insert(String sql, Class<K> pkType) {
        return insert(sql, listOf(), pkType);
    }

    @SneakyThrows
    public <K> Optional<K> insert(String sql, Object params, Class<K> pkType) {
        AtomicReference<K> key = new AtomicReference<>();
        psForParams(sql, params, (ps, args) -> {
            setStatementParams(ps, args);
            try {
                ps.executeUpdate();
            } catch (SQLException e) {
                throw Lombok.sneakyThrow(e);
            }
            if (pkType != null) {
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    rs.next();
                    key.set(uniqueValue(rs, pkType));
                } catch (Exception e) {
                    throw Lombok.sneakyThrow(e);
                }
            }
        });
        return Optional.ofNullable(key.get());
    }


    public int batchUpdate(int batchSize, String sql, List<Object> paramsList) {
        if (CollectionUtils.isEmpty(paramsList)) {
            return update(sql);
        }
        Object firstParams = paramsList.get(0);
        if (firstParams instanceof Map) {
            return batchUpdateForMapParams(batchSize, sql, (List<Map<String, Object>>) (Object) paramsList);
        } else if (firstParams instanceof List) {
            return batchUpdateForListParams(batchSize, sql, (List<List<Object>>) (Object) paramsList);
        } else {
            return batchUpdateForBeanParams(batchSize, sql, paramsList);
        }
    }

    @SneakyThrows
    protected int batchUpdateForListParams(int batchSize, String sql, List<List<Object>> paramsList) {
        if (CollectionUtils.isEmpty(paramsList)) {
            return update(sql);
        }
        List<List<List<Object>>> parts = partition(paramsList, batchSize);
        int[] count = {0};
        String actualSql = replaceEachMatched(sql, NAMED_PARAM_PATTERN, (idx, name) -> "?");
        onParsedSql(actualSql);
        try (PreparedStatement ps = getConn().prepareStatement(actualSql)) {
            for (int i = 0; i < parts.size(); i++) {
                List<List<Object>> part = parts.get(i);
                for (List<Object> params : part) {
                    setStatementParams(ps, params, false);
                    ps.addBatch();
                }
                int[] effects = ps.executeBatch();
                getConn().commit();
                count[0] += sum(effects);
            }
        }
        return count[0];
    }

    protected int sum(int[] effects) {
        if (effects == null) {
            return 0;
        }
        int count = 0;
        for (int i = 0; i < effects.length; i++) {
            count += effects[i];
        }
        return count;
    }

    @SneakyThrows
    protected int batchUpdateForBeanParams(int batchSize, String sql, List<Object> paramsList) {
        return batchUpdateForMapParams(batchSize, sql,
                paramsList.stream().map(this::beanToMap).collect(Collectors.toList()));
    }

    protected Map<String, Object> beanToMap(Object it) {
        return BeanMap.create(it);
    }

    @SneakyThrows
    protected int batchUpdateForMapParams(int batchSize, String sql, List<Map<String, Object>> paramsList) {
        if (CollectionUtils.isEmpty(paramsList)) {
            return update(sql);
        }
        List<List<Map<String, Object>>> parts = partition(paramsList, batchSize);
        int[] count = {0};
        List<String> keys = new ArrayList<>();
        String actualSql = replaceEachMatched(sql, NAMED_PARAM_PATTERN, (idx, name) -> {
            //eg:  name is :id, params.get("id")
            String key = name.substring(1);
            keys.add(key);
            return "?";
        });
        onParsedSql(actualSql);
        try (PreparedStatement ps = getConn().prepareStatement(actualSql)) {
            for (int i = 0; i < parts.size(); i++) {
                List<Map<String, Object>> part = parts.get(i);
                for (Map<String, Object> params : part) {
                    setStatementParams(ps, values(params, keys), false);
                    ps.addBatch();
                }
                int[] effects = ps.executeBatch();
                getConn().commit();
                count[0] += sum(effects);
            }
        }
        return count[0];
    }

    protected List<Object> values(Map<String, Object> params, List<String> keys) {
        return keys.stream().map(params::get).collect(Collectors.toList());
    }

    public <K> List<K> batchInsert(int batchSize, String sql, List<Object> paramsList, Class<K> pkType) {
        Object firstParams = paramsList.get(0);
        if (firstParams == null || firstParams instanceof Map) {
            return batchInsertForMapParams(batchSize, sql, (List<Map<String, Object>>) (Object) paramsList, pkType);
        } else if (firstParams instanceof List) {
            return batchInsertForListParams(batchSize, sql, (List<List<Object>>) (Object) paramsList, pkType);
        } else {
            return batchInsertForBeanParams(batchSize, sql, paramsList, pkType);
        }
    }

    @SneakyThrows
    protected <K> List<K> batchInsertForListParams(int batchSize, String sql, List<List<Object>> paramsList, Class<K> pkType) {
        if (CollectionUtils.isEmpty(paramsList)) {
            return toList(insert(sql, pkType));
        }
        List<List<List<Object>>> parts = partition(paramsList, batchSize);
        String actualSql = replaceEachMatched(sql, NAMED_PARAM_PATTERN, (idx, name) -> "?");
        List<K> pks = new ArrayList<>();
        onParsedSql(actualSql);
        try (PreparedStatement ps = getConn().prepareStatement(actualSql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < parts.size(); i++) {
                List<List<Object>> part = parts.get(i);
                for (List<Object> params : part) {
                    setStatementParams(ps, params, false);
                    ps.addBatch();
                }
                ps.executeBatch();
                getConn().commit();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    while (rs.next()) {
                        pks.add(uniqueValue(rs, pkType));
                    }
                }
            }
        }
        return pks;
    }

    @SneakyThrows
    protected <K> List<K> batchInsertForBeanParams(int batchSize, String sql, List<Object> paramsList, Class<K> pkType) {
        return batchInsertForMapParams(batchSize, sql,
                paramsList.stream().map(this::beanToMap).collect(Collectors.toList()), pkType);
    }

    protected <T> List<T> toList(Optional<T> op) {
        if (op.isPresent()) {
            return listOf(op.get());
        }
        return listOf();
    }

    @SneakyThrows
    protected <K> List<K> batchInsertForMapParams(int batchSize, String sql, List<Map<String, Object>> paramsList, Class<K> pkType) {
        if (CollectionUtils.isEmpty(paramsList)) {
            return toList(insert(sql, pkType));
        }
        List<List<Map<String, Object>>> parts = partition(paramsList, batchSize);
        List<String> keys = new ArrayList<>();
        String actualSql = replaceEachMatched(sql, NAMED_PARAM_PATTERN, (idx, name) -> {
            //eg:  name is :id, params.get("id")
            String key = name.substring(1);
            keys.add(key);
            return "?";
        });
        List<K> pks = new ArrayList<>();
        onParsedSql(actualSql);
        try (PreparedStatement ps = getConn().prepareStatement(actualSql, Statement.RETURN_GENERATED_KEYS)) {
            for (int i = 0; i < parts.size(); i++) {
                List<Map<String, Object>> part = parts.get(i);
                for (Map<String, Object> params : part) {
                    setStatementParams(ps, values(params, keys));
                    ps.addBatch();
                }
                ps.executeBatch();
                getConn().commit();
                try (ResultSet rs = ps.getGeneratedKeys()) {
                    while (rs.next()) {
                        pks.add(uniqueValue(rs, pkType));
                    }
                }
            }
        }
        return pks;
    }

    protected void onParsedSql(String actualSql) {
        onParsedSqlFn.accept(actualSql);
    }

    public static <T> List<List<T>> partition(List<T> list, int chuckSize) {
        Assert.isTrue(chuckSize > 0, "checkSize must bigger than zero!");
        List<List<T>> listOfList = new ArrayList<>();
        int total = list.size();
        for (int from = 0, to = chuckSize; from < total; from = to, to += chuckSize) {
            listOfList.add(list.subList(from, to < total ? to : total));
        }
        return listOfList;
    }

    public static <T> List<T> listOf(T... xs) {
        List<T> list = new ArrayList<>();
        if (xs == null) {
            return list;
        }
        list.addAll(Arrays.asList(xs));
        return list;
    }

    public static <K, V> Map<K, V> mapOf(Object... kvs) {
        Map<K, V> map = new HashMap<>();
        if (kvs == null) {
            return map;
        }
        Assert.isTrue(kvs.length % 2 == 0, "length of kvs must be even");
        for (int i = 0; i < kvs.length; i += 2) {
            map.put((K) kvs[i], (V) kvs[i + 1]);
        }
        return map;
    }

    public static String replaceEachMatched(String str, Pattern p, BiFunction<Integer, String, String> fn) {
        Matcher matcher = p.matcher(str);
        StringBuilder sb = new StringBuilder();
        int nth = 0;
        int start = 0;
        int end = 0;
        String group;
        while (matcher.find(start)) {
            end = matcher.end();
            group = matcher.group();
            sb.append(str, start, end - group.length()).append(fn.apply(nth++, group));
            start = end;
        }
        sb.append(str, end, str.length());
        return sb.toString();
    }
}
