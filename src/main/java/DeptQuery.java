import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Data
public class DeptQuery {
    String name;
    List<String> codes = new ArrayList<>();
    String code;
    Long idMin;
    Long idMax;
    public DeptQuery tap(Consumer<DeptQuery> fn){
        fn.accept(this);
        return this;
    }
}
