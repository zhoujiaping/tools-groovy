CREATE TABLE `t_dept` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(64) DEFAULT NULL,
  `code` varchar(64) DEFAULT NULL,
  `org_id` bigint DEFAULT NULL,
  `remark` varchar(128) DEFAULT NULL,
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP,
  `creator` bigint DEFAULT NULL,
  `updater` bigint DEFAULT NULL,
  `last_update_time` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `t_dept_idx1` (`org_id`)
) ENGINE=InnoDB AUTO_INCREMENT=1200015 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;