import org.junit.jupiter.api.Test;

import java.util.List;

public class DbCliTest {
    String url = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf-8&useSSL=false&allowPublicKeyRetrieval=true";
    String username = "root";
    String password = "123456";
    String driver = "com.mysql.cj.jdbc.Driver";

    @Test
    public void testEachBeanRow1() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.eachRowForBean("select * from t_dept where code like :code and id between :idMin and :idMax limit 10",
                    DbCli.listOf("%1%", 5, 100), () -> new Dept(), dept -> {
                        System.out.println(dept);
                    });
        });
    }

    @Test
    public void testEachBeanRow2() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.eachRowForBean("select * from t_dept where code like ? and id between ? and ? limit 10",
                    DbCli.listOf("%1%", 5, 100), () -> new Dept(), dept -> {
                        System.out.println(dept);
                    });
        });
    }

    @Test
    public void testEachBeanRow3() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.eachRowForBean("select * from t_dept where code like :code and id between :idMin and :idMax limit 10",
                    DbCli.mapOf("code", "%1%", "idMin", 5, "idMax", 100), () -> new Dept(), dept -> {
                        System.out.println(dept);
                    });
        });
    }

    @Test
    public void testEachBeanRow4() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.eachRowForBean("select * from t_dept where code like :code and id between :idMin and :idMax limit 10",
                    new DeptQuery().tap(it -> {
                        it.setCode("%1%");
                        it.setIdMin(5L);
                        it.setIdMax(100L);
                    }), () -> new Dept(), dept -> {
                        System.out.println(dept);
                    });
        });
    }

    @Test
    public void testEachBeanRow5() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.eachRowForBean("select * from t_dept where code in (:codes) and id between :idMin and :idMax limit 10",
                    new DeptQuery().tap(it -> {
                        it.setCodes(DbCli.listOf("code15", "code16"));
                        it.setIdMin(5L);
                        it.setIdMax(100L);
                    }), () -> new Dept(), dept -> {
                        System.out.println(dept);
                    });
        });
    }

    @Test
    public void testEachMapRow1() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.eachRowForMap("select * from t_dept where code in (:codes) and id between :idMin and :idMax limit 10",
                    new DeptQuery().tap(it -> {
                        it.setCodes(DbCli.listOf("code15", "code16"));
                        it.setIdMin(5L);
                        it.setIdMax(100L);
                    }), dept -> {
                        System.out.println(dept);
                    });
        });
    }

    @Test
    public void testEachListRow1() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.eachRowForList("select * from t_dept where code in (:codes) and id between :idMin and :idMax limit 10",
                    new DeptQuery().tap(it -> {
                        it.setCodes(DbCli.listOf("code15", "code16"));
                        it.setIdMin(5L);
                        it.setIdMax(100L);
                    }), dept -> {
                        System.out.println(dept);
                    });
        });
    }

    @Test
    public void testEachListRow2() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.eachRowForListWithLabel("select * from t_dept where code in (:codes) and id between :idMin and :idMax limit 10",
                    new DeptQuery().tap(it -> {
                        it.setCodes(DbCli.listOf("code15", "code16"));
                        it.setIdMin(5L);
                        it.setIdMax(100L);
                    }), dept -> {
                        System.out.println(dept);
                    });
        });
    }

    @Test
    public void testUpdate1() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.withTx(conn -> {
                int count = cli.update("update t_dept set name=:name where code in (:codes) and id between :idMin and :idMax",
                        new DeptQuery().tap(it -> {
                            it.setName("zzz");
                            it.setCodes(DbCli.listOf("code15", "code16"));
                            it.setIdMin(5L);
                            it.setIdMax(100L);
                        }));
                System.out.println(count);
            });
            Object res = cli.rowsForListWithLabel("select * from t_dept where name=:name", DbCli.listOf("zzz"));
            System.out.println(res);
        });
    }

    @Test
    public void testInsert1() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.withTx(conn -> {
                int count = cli.update("insert into t_dept(name,code)values(:name,:code)",
                        DbCli.mapOf("name", "zzzz", "code", "xxxx"));
                System.out.println(count);
            });
            Object res = cli.rowsForListWithLabel("select * from t_dept where name=:name", DbCli.listOf("zzzz"));
            System.out.println(res);
        });
    }

    @Test
    public void testBatchUpdate1() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.withTx(conn -> {
                int count = cli.batchUpdate(2, "update t_dept set name=:name where code =:code and id between :idMin and :idMax",
                        DbCli.listOf(new DeptQuery().tap(it -> {
                            it.setName("zzz");
                            it.setCode("code15");
                            it.setIdMin(5L);
                            it.setIdMax(100L);
                        }), new DeptQuery().tap(it -> {
                            it.setName("zzz");
                            it.setCode("code15");
                            it.setIdMin(5L);
                            it.setIdMax(100L);
                        }), new DeptQuery().tap(it -> {
                            it.setName("zzz");
                            it.setCode("code15");
                            it.setIdMin(5L);
                            it.setIdMax(100L);
                        })));
                System.out.println(count);
            });
            Object res = cli.rowsForListWithLabel("select * from t_dept where name=:name", DbCli.listOf("zzz"));
            System.out.println(res);
        });
    }

    @Test
    public void testBatchInsert1() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.withTx(conn -> {
                List<Long> pks = cli.batchInsert(2, "insert into t_dept(name,code)values(:name,:code)",
                        DbCli.listOf(
                                DbCli.mapOf("name", "zzzz", "code", "xxxx"),
                                DbCli.mapOf("name", "zzzz", "code", "xxxx"),
                                DbCli.mapOf("name", "zzzz", "code", "xxxx")
                        ), Long.class);
                System.out.println(pks);
            });
        });
    }

    @Test
    public void testBatchInsert2() {
        DbCli.withInstance(url, username, password, driver, cli -> {
            cli.withTx(conn -> {
                List<Long> pks = cli.batchInsert(2, "insert into t_dept(name,code)values(:name,:code)",
                        DbCli.listOf(
                                DbCli.listOf("zzzz", "xxxx"),
                                DbCli.listOf("zzzz", "xxxx"),
                                DbCli.listOf("zzzz", "xxxx")
                        ), Long.class);
                System.out.println(pks);
            });
        });
    }
}
