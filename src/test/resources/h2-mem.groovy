import groovy.sql.Sql

/**
 * 场景 有些数据分析任务需要频繁访问数据库
 * http://www.h2database.com/html/tutorial.html
 */
def client = Sql.newInstance('jdbc:h2:mem:test_mem', '', '', 'org.h2.Driver')

client.execute('''
create table t_user(
    id bigint primary key,
    name varchar(50)
);
''')
client.execute('''
create index t_user_n1 on t_user(name);
''')
client.execute('''
insert into t_user(id,name)values(1,'zhou'),(2,'zhu');
''')
client.eachRow('select * from t_user'){
    println it['id']
    println it['name']
}
client.eachRow('''
explain
select * from t_user where name like 'z%'
'''){
    println it
}